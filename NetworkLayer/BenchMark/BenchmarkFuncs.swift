//
//  BenchmarkFuncs.swift
//  NetworkLayer
//
//  Created by Dmitriy Ignatyev on 11.11.2017.
//  Copyright © 2017 Dmitriy Ignatyev. All rights reserved.
//

import Foundation

@discardableResult
func performBenchmark(name: String, count: Int = 1, _ actions: () -> Void) -> Double {
  guard count > 0 else {
    print("Benchmark should be performed at least 1 time")
    return 0
  }
  var totalInterval: CFTimeInterval = 0
  var minInterval: CFTimeInterval = CFTimeInterval.infinity
  var maxInterval: CFTimeInterval = -CFTimeInterval.infinity
  var counter: Int = 0
  
  while counter < count {
    let initialTime = CFAbsoluteTimeGetCurrent()
    actions()
    let endTime = CFAbsoluteTimeGetCurrent()
    let difference = endTime - initialTime
    totalInterval += difference
    
    difference < minInterval ? (minInterval = difference) : ()
    difference > maxInterval ? (maxInterval = difference) : ()
    
    counter += 1
  }
  
  let averageInterval = count == 1 ? totalInterval : (totalInterval / CFTimeInterval(count))
  
  print("\n\(name) benchmark: \ntotal  : \(benchString(totalInterval)) s.")
  
  if count > 1 {
    print("average: \(benchString(averageInterval)) s.")
    print("minTime: \(benchString(minInterval)) s.")
    print("maxTime: \(benchString(maxInterval)) s.")
  }
  
  return averageInterval
}

func performMeasuredAction(name: String, _ actions: () -> Void) {
  let initialTime = CFAbsoluteTimeGetCurrent()
  actions()
  let endTime = CFAbsoluteTimeGetCurrent()
  let difference = endTime - initialTime
  print("\n\(name) action completed in \(benchString(difference)) s.")
}

func benchString(_ double: Double) -> String {
  return String(format: "%.9f", double)
}




