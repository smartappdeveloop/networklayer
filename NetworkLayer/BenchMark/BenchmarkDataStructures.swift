//
//  BenchmarkDataStructures.swift
//  NetworkLayer
//
//  Created by Dmitriy Ignatyev on 11.11.2017.
//  Copyright © 2017 Dmitriy Ignatyev. All rights reserved.
//

import Foundation
import ObjectMapper

struct PassengerJsonModel: ImmutableMappable {
    let id         : Int64
    let clientId   : Int64
    
    let firstName  : String
    let lastName   : String
    let middleName : String?
    
    let gender     : String?
    let birthDate  : Date?
    let pseudonym  : String
    
    let documents  : [PassengerDocumentJsonModel]
    
    let abstract: Decodable = 5
    
    init(id: Int64, clientId: Int64, firstName: String, lastName: String, middleName: String?, gender: String?,
         birthDate: Date?, pseudonym: String, documents: [PassengerDocumentJsonModel]) {
        self.id         = id
        self.clientId   = clientId
        self.firstName  = firstName
        self.lastName   = lastName
        self.middleName = middleName
        self.gender     = gender
        self.birthDate  = birthDate
        self.pseudonym  = pseudonym
        self.documents  = documents
    }
    
    // Mapping
    init(map: Map) throws {
        id         = try map.value("ID")
        clientId   = try map.value("ClientId")
        
        firstName  = try map.value("FirstName")
        lastName   = try map.value("LastName")
        middleName = try? map.value("MiddleName")
        
        gender     = try? map.value("Gender")
        birthDate  = try? map.value("DateBirth",
                                    using : DateFormatterTransform(dateFormatter : PassengerJsonModel.birthDateServerFormatter))
        pseudonym  = try map.value("Name")
        documents  = try map.value("IdentityCertificates")
    }
    
    func mapping(map: Map) {
        map.shouldIncludeNilValues = true
        id         >>> map["ID"]
        clientId   >>> map["ClientId"]
        firstName  >>> map["FirstName"]
        lastName   >>> map["LastName"]
        middleName >>> map["MiddleName"]
        gender     >>> map["Gender"]
        birthDate  >>> (map["DateBirth"],
                        DateFormatterTransform(dateFormatter: PassengerJsonModel.birthDateServerFormatter))
        pseudonym  >>> map["Name"]
        documents  >>> map["IdentityCertificates"]
    }
    
    static var jsonEncoder: JSONEncoder {
        // Если делать как static let, то можно поменять dateEncodingStrategy или иные параметры
        let encoder = JSONEncoder()
        encoder.dateEncodingStrategy = .formatted(PassengerJsonModel.birthDateServerFormatter)
        return encoder
    }
}


struct DecodingValueApplier<K> where K: CodingKey {
    let container: KeyedDecodingContainer<K>
    
    init(container: KeyedDecodingContainer<K>) {
        self.container = container
    }
    
    func value<V>(for key: K) throws -> V where V: Decodable {
        let value = try container.decode(V.self, forKey: key)
        return value
    }
}

struct EncodingValueExtractor<K> where K: CodingKey {
    let container: KeyedEncodingContainer<K>
    
    init(container: KeyedEncodingContainer<K>, includeNullValues: Bool = true, dateFromatter: DateFormatter) {
        self.container = container
    }
    
    func encode<V>(_ value: V, key: K) throws where V: Encodable  {
        // TODO: Implement
    }
}





extension PassengerJsonModel: Codable {
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        
        //    id = try container.decode(Int64.self, forKey: .id)
        //    clientId = try container.decode(Int64.self, forKey: .clientId)
        //    firstName = try container.decode(String.self, forKey: .firstName)
        //    lastName = try container.decode(String.self, forKey: .lastName)
        //    middleName = try container.decode(String?.self, forKey: .middleName)
        //    gender = try container.decode(String?.self, forKey: .gender)
        //    birthDate = try container.decode(Date?.self, forKey: .birthDate)
        //    pseudonym = try container.decode(String.self, forKey: .pseudonym)
        //    documents = try container.decode(Array<PassengerDocumentJsonModel>.self, forKey: .documents)
        
        let applier = DecodingValueApplier(container: container)
        
        id = try applier.value(for: .id)
        clientId = try applier.value(for: .clientId)
        firstName = try applier.value(for: .firstName)
        lastName = try applier.value(for: .lastName)
        middleName = try applier.value(for: .middleName)
        gender = try applier.value(for: .gender)
        birthDate = try applier.value(for: .birthDate)
        pseudonym = try applier.value(for: .pseudonym)
        documents = try applier.value(for: .documents)
        
        
        let clientIdPath = \PassengerJsonModel.clientId
        let abstractPath = \PassengerJsonModel.abstract
        //let abstractPath = WritableKeyPath<PassengerJsonModel, Codable>()
        let value = self[keyPath: abstractPath]
        
        
    }
    
    func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)
        
        try container.encode(id, forKey: .id)
        try container.encode(clientId, forKey: .clientId)
        try container.encode(firstName, forKey: .firstName)
        try container.encode(lastName, forKey: .lastName)
        try container.encode(middleName, forKey: .middleName)
        try container.encode(gender, forKey: .gender)
        try container.encode(birthDate, forKey: .birthDate)
        try container.encode(pseudonym, forKey: .pseudonym)
        try container.encode(documents, forKey: .documents)
    }
    
    enum CodingKeys: String, CodingKey {
        case id = "ID"
        case clientId = "ClientId"
        case firstName = "FirstName"
        case lastName = "LastName"
        case middleName = "MiddleName"
        case gender = "Gender"
        case birthDate = "DateBirth"
        case pseudonym = "Name"
        case documents = "IdentityCertificates"
    }
}

extension PassengerJsonModel {
    static let birthDateServerFormat = "yyyy-MM-dd'T'HH:mm:ss"
    
    static let birthDateServerFormatter: DateFormatter = {
        return PassengerJsonModel.getDateFormatterWith(format: birthDateServerFormat)
    }()
    
    static func getDateFormatterWith(format: String) -> DateFormatter {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = format
        return dateFormatter
    }
}



struct PassengerDocumentJsonModel: ImmutableMappable { //
    let id             : Int64
    let passengerId    : Int64
    
    let firstNameRu    : String?
    let lastNameRu     : String?
    let middleNameRu   : String?
    
    let firstNameEn    : String?
    let lastNameEn     : String?
    let middleNameEn   : String?
    
    let isoCountryCode : String?
    let сountryName    : String?
    
    let typeSysName    : String
    
    let number         : String
    let expireDate     : Date?
    
    init(id: Int64, passengerId: Int64, firstNameRu: String?, lastNameRu: String?, middleNameRu: String?,
         firstNameEn: String?, lastNameEn: String?, middleNameEn: String?,
         isoCountryCode: String?, сountryName: String?, typeSysName: String, number: String, expireDate: Date?) {
        self.id             = id
        self.passengerId    = passengerId
        self.firstNameRu    = firstNameRu
        self.lastNameRu     = lastNameRu
        self.middleNameRu   = middleNameRu
        self.firstNameEn    = firstNameEn
        self.lastNameEn     = lastNameEn
        self.middleNameEn   = middleNameEn
        self.isoCountryCode = isoCountryCode
        self.сountryName    = сountryName
        self.typeSysName    = typeSysName
        
        self.number         = number
        self.expireDate     = expireDate
    }
    
    
    // ImmutableMappable
    init(map: Map) throws {
        id             = try map.value("ID")
        passengerId    = try map.value("PersonHumanClientID")
        firstNameRu    = try? map.value("FirstNameDocRu")
        lastNameRu     = try? map.value("LastNameDocRu")
        middleNameRu   = try? map.value("MiddleNameDocRu")
        firstNameEn    = try? map.value("FirstNameDocEn")
        lastNameEn     = try? map.value("LastNameDocEn")
        middleNameEn   = try? map.value("MiddleNameDocEn")
        isoCountryCode = try? map.value("CountryCode")
        сountryName    = try? map.value("CountryName")
        typeSysName    = try map.value("TypeSysName")
        number         = try map.value("Name")
        expireDate     = try? map.value("DateOut",
                                        using: DateFormatterTransform(dateFormatter: PassengerJsonModel.birthDateServerFormatter))
    }
    
    func mapping(map: Map) {
        map.shouldIncludeNilValues = true
        id             >>> map["ID"]
        passengerId    >>> map["PersonHumanClientID"]
        
        firstNameRu    >>> map["FirstNameDocRu"]
        lastNameRu     >>> map["LastNameDocRu"]
        middleNameRu   >>> map["MiddleNameDocRu"]
        
        firstNameEn    >>> map["FirstNameDocEn"]
        lastNameEn     >>> map["LastNameDocEn"]
        middleNameEn   >>> map["MiddleNameDocEn"]
        
        isoCountryCode >>> map["CountryCode"]
        сountryName    >>> map["CountryName"]
        
        typeSysName    >>> map["TypeSysName"]
        number         >>> map["Name"]
        expireDate     >>> (map["DateOut"],
                            DateFormatterTransform(dateFormatter: PassengerJsonModel.birthDateServerFormatter))
    }
}


extension PassengerDocumentJsonModel: Codable { //}:  {
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        
        let allKeys = container.allKeys
        
        
        id = try container.decode(Int64.self, forKey: .id)
        passengerId = try container.decode(Int64.self, forKey: .passengerId)
        firstNameRu = try container.decode(String?.self, forKey: .firstNameRu)
        lastNameRu = try container.decode(String?.self, forKey: .lastNameRu)
        middleNameRu = try container.decode(String?.self, forKey: .middleNameRu)
        firstNameEn = try container.decode(String?.self, forKey: .firstNameEn)
        lastNameEn = try container.decode(String?.self, forKey: .lastNameEn)
        middleNameEn = try container.decode(String?.self, forKey: .middleNameEn)
        isoCountryCode = try container.decode(String?.self, forKey: .isoCountryCode)
        сountryName = try container.decode(String?.self, forKey: .сountryName)
        typeSysName = try container.decode(String.self, forKey: .typeSysName)
        number = try container.decode(String.self, forKey: .number)
        expireDate = try container.decode(Date?.self, forKey: .expireDate)
        
    }
    
    func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)
        
        //    let allKeys = [CodingKeys]()
        //    for key in allKeys {
        //      switch key {
        //      case .id: break
        //      case .passengerId: break
        //      case .firstNameRu: break
        //      case .lastNameRu: break
        //      case .middleNameRu: break
        //      case .firstNameEn: break
        //      case .lastNameEn: break
        //      case .middleNameEn: break
        //      case .isoCountryCode: break
        //      case .сountryName: break
        //      case .typeSysName: break
        //      case .number: break
        //      case .expireDate: break
        //      }
        //    }
        let codingPath = container.codingPath
        
        try container.encode(id, forKey: .id)
        try container.encode(passengerId, forKey: .passengerId)
        try container.encode(firstNameRu, forKey: .firstNameRu)
        try container.encode(lastNameRu, forKey: .lastNameRu)
        try container.encode(middleNameRu, forKey: .middleNameRu)
        try container.encode(firstNameEn, forKey: .firstNameEn)
        try container.encode(lastNameEn, forKey: .lastNameEn)
        try container.encode(middleNameEn, forKey: .middleNameEn)
        try container.encode(isoCountryCode, forKey: .isoCountryCode)
        try container.encode(сountryName, forKey: .сountryName)
        try container.encode(typeSysName, forKey: .typeSysName)
        try container.encode(number, forKey: .number)
        try container.encode(expireDate, forKey: .expireDate)
    }
    
    enum CodingKeys: String, CodingKey {
        case id = "ID"
        case passengerId = "PersonHumanClientID"
        case firstNameRu = "FirstNameDocRu"
        case lastNameRu = "LastNameDocRu"
        case middleNameRu = "MiddleNameDocRu"
        case firstNameEn = "FirstNameDocEn"
        case lastNameEn = "LastNameDocEn"
        case middleNameEn = "MiddleNameDocEn"
        case isoCountryCode = "CountryCode"
        case сountryName = "CountryName"
        case typeSysName = "TypeSysName"
        case number = "Name"
        case expireDate = "DateOut"
    }
}









