//
//  JsonBenchmark.swift
//  NetworkLayer
//
//  Created by Dmitriy Ignatyev on 11.11.2017.
//  Copyright © 2017 Dmitriy Ignatyev. All rights reserved.
//

import Foundation
import ObjectMapper

struct BenchMark {
    
    static func codableVsMappable() {
        func createDocument(id: Int64, passengerId: Int64) -> PassengerDocumentJsonModel {
            let document =
                PassengerDocumentJsonModel(id: id, passengerId: passengerId,
                                           firstNameRu: "Константин\(id)", lastNameRu: "Константинов\(passengerId)", middleNameRu: "Иванович",
                                           firstNameEn: "", lastNameEn: "", middleNameEn: nil,
                                           isoCountryCode: "RU", сountryName: "Russia",
                                           typeSysName: "InternalPassport", number: "1099099890", expireDate: nil)
            return document
        }
        func createPassenger(id: Int64) -> PassengerJsonModel {
            var docs = [PassengerDocumentJsonModel]()
            var i: Int64 = 0
            while i < 2 {
                docs.append(createDocument(id: i, passengerId: id))
                i += 1
            }
            let passenger = PassengerJsonModel(id: id, clientId: 0,
                                               firstName: "Константин", lastName: "Константинов", middleName: "Иванович",
                                               gender: "Male", birthDate: Date(),
                                               pseudonym: "Константинов", documents: docs)
            return passenger
        }
        
        var jsonModels = [PassengerJsonModel]()
        performMeasuredAction(name: "prepare Json Objects") {
            var i: Int64 = 0
            while i < 100 {
                jsonModels.append(createPassenger(id: i))
                i += 1
            }
        }
        
        let benchCount: Int = 3
        
        guard let allPassengersJsonString: String = jsonModels.toJSONString() else { return }
        let allPaasengersJsonArray: [[String : Any]] = jsonModels.toJSON()
        
        // Encoding
        let jsonEncoder = JSONEncoder()
        jsonEncoder.dateEncodingStrategy = .formatted(PassengerJsonModel.birthDateServerFormatter)
        
        // Decodibg
        let jsonDecoder = JSONDecoder()
        jsonDecoder.dateDecodingStrategy = .formatted(PassengerJsonModel.birthDateServerFormatter)
        
        do {
            guard let passengerModel = jsonModels.first else { return }
            guard let passengerModelJsonString = passengerModel.toJSONString(prettyPrint: true) else { return }
            guard let documentModel = passengerModel.documents.first else { return }
            guard let documentModelJsonString = documentModel.toJSONString(prettyPrint: true) else { return }
            
            // Encoding
            guard let passengerModelData = try? passengerModel.encodedData(using: jsonEncoder) else { return }
            let passengerModelDataUtf = passengerModelData.utf8String
            
            // Decodibg
            guard let passengerModelDecoded = try? PassengerJsonModel.from(data: passengerModelData, using: jsonDecoder) else { return }
            
            let codingKey = PassengerJsonModel.CodingKeys(intValue: 8)
            print(6)
        }
        
        guard let allPaasengersJsonData: Data = try? jsonModels.encodedData(using: jsonEncoder) else { return }
        guard let jsonObjectAny = try? JSONSerialization.jsonObject(with: allPaasengersJsonData, options: .allowFragments) else { return }
        guard let jsonObject = jsonObjectAny as? Array<Dictionary<String, Any>> else { return }
        
        performBenchmark(name: "from Json Array ObjectMapper", count: benchCount) {
            _ = try? Mapper<PassengerJsonModel>().mapArray(JSONArray: allPaasengersJsonArray)
        }
        performBenchmark(name: "from Json Array Encodable", count: benchCount) {
            guard let data: Data = try? JSONSerialization.data(withJSONObject: allPaasengersJsonArray, options: []) else { return }
            _ = try? Array<PassengerJsonModel>.from(data: data, using: jsonDecoder)
        }
        performBenchmark(name: "from String ObjectMapper", count: benchCount) {
            _ = try? Mapper<PassengerJsonModel>().mapArray(JSONString: allPassengersJsonString)
        }
        performBenchmark(name: "from String Encodable", count: benchCount) {
            _ = try? Array<PassengerJsonModel>.from(jsonString: allPassengersJsonString, stringEncoding: .utf8, using: jsonDecoder)
        }
        performBenchmark(name: "to Json Array ObjectMapper", count: benchCount) {
            _ = jsonModels.toJSON()
        }
        performBenchmark(name: "to Json Array Decodable", count: benchCount) {
            guard let data = try? jsonModels.encodedData(using: jsonEncoder) else { return }
            let arrayAny = try? JSONSerialization.jsonObject(with: data, options: [])
            _ = arrayAny as? [[String : Any]]
        }
        performBenchmark(name: "to Json String ObjectMapper", count: benchCount) {
            _ = jsonModels.toJSONString()
        }
        performBenchmark(name: "to Json String Decodable", count: benchCount) {
            _ = try? jsonModels.utf8JsonString(using: jsonEncoder)
        }
        
        performBenchmark(name: "to Json Data Encodable", count: benchCount) {
            _ = try? jsonModels.encodedData(using: jsonEncoder)
        }
    }
    
}

/*
 
 50 * 1000
 from Json Array ObjectMapper benchmark:
 total  : 7.175226986 s.
 average: 0.143504540 s.
 minTime: 0.132408023 s.
 maxTime: 0.171121001 s.
 
 from Json Array Encodable benchmark:
 total  : 10.009001017 s.
 average: 0.200180020 s.
 minTime: 0.188627005 s.
 maxTime: 0.223865986 s.
 
 from String ObjectMapper benchmark:
 total  : 11.549189985 s.
 average: 0.230983800 s.
 minTime: 0.220386982 s.
 maxTime: 0.277576029 s.
 
 from String Encodable benchmark:
 total  : 8.479938984 s.
 average: 0.169598780 s.
 minTime: 0.159356952 s.
 maxTime: 0.218017042 s.
 
 to Json Array ObjectMapper benchmark:
 total  : 3.258233905 s.
 average: 0.065164678 s.
 minTime: 0.061764002 s.
 maxTime: 0.076847970 s.
 
 to Json Array Decodable benchmark:
 total  : 7.414704263 s.
 average: 0.148294085 s.
 minTime: 0.138553023 s.
 maxTime: 0.181380033 s.
 
 to Json String ObjectMapper benchmark:
 total  : 7.866827965 s.
 average: 0.157336559 s.
 minTime: 0.143127978 s.
 maxTime: 0.172605038 s.
 
 to Json String Decodable benchmark:
 total  : 6.875161052 s.
 average: 0.137503221 s.
 minTime: 0.129541039 s.
 maxTime: 0.159484982 s.
 
 to Json Data Encodable benchmark:
 total  : 6.978690922 s.
 average: 0.139573818 s.
 minTime: 0.125837028 s.
 maxTime: 0.200294018 s.
 
 
 
 // 30 * 10000
 
 from Json Array ObjectMapper benchmark:
 total  : 42.975541055 s.
 average: 1.432518035 s.
 minTime: 1.395476997 s.
 maxTime: 1.626229048 s.
 
 from Json Array Encodable benchmark:
 total  : 61.104694963 s.
 average: 2.036823165 s.
 minTime: 1.999962032 s.
 maxTime: 2.092688978 s.
 
 from String ObjectMapper benchmark:
 total  : 72.166342914 s.
 average: 2.405544764 s.
 minTime: 2.364774048 s.
 maxTime: 2.443204999 s.
 
 from String Encodable benchmark:
 total  : 51.863530755 s.
 average: 1.728784359 s.
 minTime: 1.699877977 s.
 maxTime: 1.844776034 s.
 
 to Json Array ObjectMapper benchmark:
 total  : 20.146784961 s.
 average: 0.671559499 s.
 minTime: 0.648850977 s.
 maxTime: 0.716410995 s.
 
 to Json Array Decodable benchmark:
 total  : 43.670313060 s.
 average: 1.455677102 s.
 minTime: 1.413067997 s.
 maxTime: 1.553579032 s.
 
 to Json String ObjectMapper benchmark:
 total  : 45.425344944 s.
 average: 1.514178165 s.
 minTime: 1.410148025 s.
 maxTime: 1.620442033 s.
 
 to Json String Decodable benchmark:
 total  : 40.434233129 s.
 average: 1.347807771 s.
 minTime: 1.326348007 s.
 maxTime: 1.389616966 s.
 
 to Json Data Encodable benchmark:
 total  : 38.780314803 s.
 average: 1.292677160 s.
 minTime: 1.262979031 s.
 maxTime: 1.348576009 s.
 
 */



