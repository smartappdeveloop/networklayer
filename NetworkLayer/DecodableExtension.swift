//
//  Extensions.swift
//  NetworkLayer
//
//  Created by Dmitriy Ignatyev on 16.11.2017.
//  Copyright © 2017 Dmitriy Ignatyev. All rights reserved.
//

import Foundation

extension Data {
  var utf8String: String? {
    return string(as: .utf8)
  }
  
  func string(as encoding: String.Encoding) -> String? {
    return String(data: self, encoding: encoding)
  }
}

// MARK: - Encodable
extension Encodable {
//  private static func encodedData<T>(of object: T, using encoder: JSONEncoder = JSONEncoder()) throws -> Data where T: Encodable {
//    let data = try encoder.encode(object)
//    return data
//  }
//  func encodedData(using encoder: JSONEncoder = JSONEncoder()) throws -> Data {
//    let data = try Self.encodedData(of: self, using: encoder)
//    return data
//  }
  func encodedData(using encoder: JSONEncoder = JSONEncoder()) throws -> Data {
    let data: Data = try encoder.encode(self)
    return data
  }
  
//  private static func encodedData<T>(of object: T,
//                                     configuringEncoder: (_ encoder: JSONEncoder) -> Void) throws -> Data where T: Encodable {
//    let encoder = JSONEncoder()
//    configuringEncoder(encoder)
//    let jsonData = try Self.encodedData(of: object, using: encoder)
//    return jsonData
//  }
//  func encodedData(configuringEncoder: (_ encoder: JSONEncoder) -> Void) throws -> Data {
//    let data = try Self.encodedData(of: self, configuringEncoder: configuringEncoder)
//    return data
//  }
  func encodedData(configuringEncoder: (_ encoder: JSONEncoder) -> Void) throws -> Data {
    let encoder = JSONEncoder()
    configuringEncoder(encoder)
    let data: Data = try encoder.encode(self)
    return data
  }
  
  func utf8JsonString(using encoder: JSONEncoder = JSONEncoder()) throws -> String {
    let data: Data = try encoder.encode(self)
    guard let jsonString = data.string(as: .utf8) else {
      throw JSONEncoder.EncodingToStringError(description: "unable to create UTF8 json string from data")
    }
    return jsonString
  }
  
  func utf8JsonString(configuringEncoder: (_ encoder: JSONEncoder) -> Void) throws -> String {
    let encoder = JSONEncoder()
    configuringEncoder(encoder)
    let jsonString = try utf8JsonString(using: encoder)
    return jsonString
  }
  
  //  func toUtf8JsonString(encoder: JSONEncoder = JSONEncoder()) -> String? {
  //      Конфликутет с throwable вариантом, нужно при объявлении указывать явно тип String?
  ////    guard let data: Data = try? encoder.encode(self) else { return nil }
  ////    return data.string(as: .utf8)
  //    let jsonString = try? toUtf8JsonString(encoder: encoder)
  //    return jsonString //
  //  }
  
  // Не работает
  //  func encodedData(using encoder: JSONEncoder = JSONEncoder()) throws -> Data {
  //    let data = try encoder.encode(self)
  //    return data
  //  }
  //
  //  func encodedData(configuringEncoder: (_ encoder: JSONEncoder) -> Void) throws -> Data {
  //    let encoder = JSONEncoder()
  //    configuringEncoder(encoder)
  //    let jsonData = try encodedData(using: encoder)
  //    return jsonData
  //  }
}

extension JSONEncoder {
  struct EncodingToStringError: Error {
    private let errorDescription: String
    init(description: String) {
      errorDescription = description
    }
    
    var localizedDescription: String {
      return errorDescription
    }
  }
}



// MARK: - Decodable
extension Decodable {
  static func from(jsonString: String, stringEncoding: String.Encoding, using decoder: JSONDecoder = JSONDecoder()) throws -> Self {
    guard let data = jsonString.data(using: stringEncoding) else {
      let errorText = "Failed to decode Data from json string in encoding \(stringEncoding)"
      throw JSONDecoder.DecodingFromStringError(description: errorText)
    }
    let decodedObject = try decoder.decode(Self.self, from: data)
    return decodedObject
  }
  
  static func from(data: Data, using decoder: JSONDecoder = JSONDecoder()) throws -> Self {
    let decodedObject = try decoder.decode(Self.self, from: data)
    return decodedObject
  }
  
}
extension JSONDecoder {
  struct DecodingFromStringError: Error {
    private let errorDescription: String
    init(description: String) {
      errorDescription = description
    }
    
    var localizedDescription: String {
      return errorDescription
    }
  }
}


// MARK: Enum

// enum which provides a count of its cases
public protocol CaseCountable {
  static var caseCount : Int { get }
}

// provide a default implementation to count the cases for Int enums assuming starting at 0 and contiguous
public extension CaseCountable where Self : RawRepresentable, Self.RawValue == Int {
  
  // count the number of cases in the enum
  public static var caseCount: Int {
    // starting at zero, verify whether the enum can be instantiated from the Int and increment until it cannot
    var count = 0
    while let _ = Self(rawValue: count) { count += 1 }
    return count
  }
  
  public static var allCases: [Self] {
    var allCases = [Self]()
    var count = 0
    while let someCase = Self(rawValue: count) {
      allCases.append(someCase)
      count += 1
    }
    return allCases
  }
}



