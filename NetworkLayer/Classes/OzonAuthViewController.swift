//
//  OzonAuthViewController.swift
//  NetworkLayer
//
//  Created by Dmitriy Ignatyev on 12.11.2017.
//  Copyright © 2017 Dmitriy Ignatyev. All rights reserved.
//

import UIKit

final class OzonAuthViewController: UIViewController {
  
  @IBOutlet weak var loginTextField: UITextField!
  @IBOutlet weak var passwordTextField: UITextField!
  
  @IBOutlet weak var responseTextView: UITextView! // Server Response Output
  
  
  override func viewDidLoad() {
    super.viewDidLoad()
    view.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(viewTapGestureAction)))
  }
  
  
  @IBAction func signInButtonAction() {
    guard let login = loginTextField.text, let password = passwordTextField.text else { return }
    
    // Authorization code
  }
  
  
  @objc func viewTapGestureAction() {
    view?.endEditing(true)
  }
  
}



