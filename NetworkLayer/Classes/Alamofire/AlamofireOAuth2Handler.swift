//
//  AlamofireOAuth2Handler.swift
//  NetworkLayer
//
//  Created by Dmitriy Ignatyev on 12.11.2017.
//  Copyright © 2017 Dmitriy Ignatyev. All rights reserved.
//

import Alamofire

/*
 DISCLAIMER: This is NOT a global OAuth2 solution. It is merely an example demonstrating how one could use the RequestAdapter in conjunction with the RequestRetrier to create a thread-safe refresh system.
 
 To reiterate, do NOT copy this sample code and drop it into a production application. This is merely an example. Each authentication system must be tailored to a particular platform and authentication type.
 */

// MARK: - Original
final class OAuth2Handler: RequestAdapter, RequestRetrier {
  private typealias RefreshCompletion = (_ succeeded: Bool, _ accessToken: String?, _ refreshToken: String?) -> Void
  private typealias RefreshTokenCompletion = (Result<(accessToken: String, refreshToken: String)>) -> Void
  
  private let sessionManager: SessionManager = {
    let configuration = URLSessionConfiguration.default
    configuration.httpAdditionalHeaders = SessionManager.defaultHTTPHeaders
    
    return SessionManager(configuration: configuration)
  }()
  
  private let lock = NSLock()
  
  private var clientID: String
  private var baseURLString: String
  private var accessToken: String
  private var refreshToken: String
  
  private var isRefreshing = false
  private var requestsToRetry: [RequestRetryCompletion] = []
  
  // MARK: Initialization
  
  public init(clientID: String, baseURLString: String, accessToken: String, refreshToken: String) {
    self.clientID = clientID
    self.baseURLString = baseURLString
    self.accessToken = accessToken
    self.refreshToken = refreshToken
  }
  
  // MARK: RequestAdapter
  
  func adapt(_ urlRequest: URLRequest) throws -> URLRequest {
    if let urlString = urlRequest.url?.absoluteString, urlString.hasPrefix(baseURLString) {
      var urlRequest = urlRequest
      urlRequest.setValue("Bearer " + accessToken, forHTTPHeaderField: "Authorization")
      return urlRequest
    }
    
    return urlRequest
  }
  
  // MARK: RequestRetrier
  
  func should(_ manager: SessionManager,
              retry request: Request,
              with error: Error,
              completion: @escaping RequestRetryCompletion) {
    lock.lock()
    defer { lock.unlock() }
    
    if let response = request.task?.response as? HTTPURLResponse, response.statusCode == 401 {
      requestsToRetry.append(completion)
      
      if !isRefreshing {
        refreshTokens { [weak self] succeeded, accessToken, refreshToken in
          guard let strongSelf = self else { return }
          
          strongSelf.lock.lock()
          defer { strongSelf.lock.unlock() }
          
          if let accessToken = accessToken, let refreshToken = refreshToken {
            strongSelf.accessToken = accessToken
            strongSelf.refreshToken = refreshToken
          }
          
          strongSelf.requestsToRetry.forEach { $0(succeeded, 0.0) }
          strongSelf.requestsToRetry.removeAll()
        }
      }
    } else {
      completion(false, 0.0)
    }
  }
  
  // MARK: Private - Refresh Tokens
  
  private func refreshTokens(completion: @escaping RefreshCompletion) {
    guard !isRefreshing else { return }
    
    isRefreshing = true
    let urlString = "\(baseURLString)/oauth2/token"
    
    let parameters: [String: Any] = [
      "access_token": accessToken,
      "refresh_token": refreshToken,
      "client_id": clientID,
      "grant_type": "refresh_token"
    ]
    
    sessionManager.request(urlString,
                           method: .post,
                           parameters: parameters,
                           encoding: JSONEncoding.default).responseJSON { [weak self] response in
                            guard let strongSelf = self else { return }
                            
                            if let json = response.result.value as? [String: Any],
                              let accessToken = json["access_token"] as? String,
                              let refreshToken = json["refresh_token"] as? String {
                              completion(true, accessToken, refreshToken)
                            } else {
                              completion(false, nil, nil)
                            }
                            
                            strongSelf.isRefreshing = false
    }
  }
}


// MARK: - Modified Variant
final class OOAuth2Handler: RequestAdapter, RequestRetrier {
  private typealias RefreshCompletion = (Result<(accessToken: String, refreshToken: String)>) -> Void
  
  private let sessionManager: SessionManager = {
    let configuration = URLSessionConfiguration.default
    configuration.httpAdditionalHeaders = SessionManager.defaultHTTPHeaders
    return SessionManager(configuration: configuration)
  }()
  
  private let lock = NSLock()
  
  private var clientID: String
  private var baseURLString: String
  private var accessToken: String
  private var refreshToken: String
  
  private var isRefreshing = false
  private var requestsToRetry: [RequestRetryCompletion] = []
  
  // MARK: Initialization
  
  public init(clientID: String, baseURLString: String, accessToken: String, refreshToken: String) {
    self.clientID = clientID
    self.baseURLString = baseURLString
    self.accessToken = accessToken
    self.refreshToken = refreshToken
  }
  
  // MARK: RequestAdapter
  
  func adapt(_ urlRequest: URLRequest) throws -> URLRequest {
    if let urlString = urlRequest.url?.absoluteString, urlString.hasPrefix(baseURLString) {
      var urlRequest = urlRequest
      urlRequest.setValue("Bearer " + accessToken, forHTTPHeaderField: "Authorization")
      return urlRequest
    }
    
    return urlRequest
  }
  
  // MARK: RequestRetrier
  
  func should(_ manager: SessionManager,
              retry request: Request,
              with error: Error,
              completion: @escaping RequestRetryCompletion) {
    lock.lock()
    defer { lock.unlock() }
    
    if let response = request.task?.response as? HTTPURLResponse, response.statusCode == 401 {
      requestsToRetry.append(completion)
      
      if !isRefreshing {
        refreshTokens { [weak self] result in
          guard let strongSelf = self else { return }
          strongSelf.lock.lock()
          defer { strongSelf.lock.unlock() }
          
          let succeeded: Bool
          switch result {
          case .failure(_): succeeded = false
          case .success(let value):
            strongSelf.accessToken = value.accessToken
            strongSelf.refreshToken = value.refreshToken
            succeeded = true
          }
          
          strongSelf.requestsToRetry.forEach { $0(succeeded, 0.0) } // FIXME: ???
          strongSelf.requestsToRetry.removeAll()
        }
      }
    } else {
      completion(false, 0.0)
    }
  }
  
  // MARK: Private - Refresh Tokens
  
  // Private - Refresh Tokens | Little bit midification
  private func refreshTokens(completion: @escaping RefreshCompletion) {
    guard !isRefreshing else { return }
    
    isRefreshing = true
    
    let urlString = "\(baseURLString)/oauth2/token"
    
    let parameters: [String: Any] = [
      "access_token": accessToken,
      "refresh_token": refreshToken,
      "client_id": clientID,
      "grant_type": "refresh_token"
    ]
    
    sessionManager.request(urlString,
                           method: .post,
                           parameters: parameters,
                           encoding: JSONEncoding.default)
      .responseJSON { [weak self] response in
        guard let strongSelf = self else { return }
        
        switch response.result {
        case .failure(let error): completion(.failure(error))
        case .success(let value):
          if let json = value as? [String: Any],
            let accessToken = json["access_token"] as? String,
            let refreshToken = json["refresh_token"] as? String {
            completion(.success((accessToken: accessToken, refreshToken: refreshToken)))
          } else {
            let error = BackendError.objectSerialization(reason: "unable to extract token from response")
            completion(.failure(error))
          }
        }
        
        strongSelf.isRefreshing = false
    }
  }
  
}



func oauthPlay() {
  let baseURLString = "https://some.domain-behind-oauth2.com"
  
  let oauthHandler = OAuth2Handler(
    clientID: "12345678",
    baseURLString: baseURLString,
    accessToken: "abcd1234",
    refreshToken: "ef56789a"
  )
  
  let sessionManager = SessionManager()
  sessionManager.adapter = oauthHandler
  sessionManager.retrier = oauthHandler
  
  let urlString = "\(baseURLString)/some/endpoint"
  
  sessionManager.request(urlString).validate().responseJSON { response in
    debugPrint(response)
  }
}



/*
If you needed them to execute in the same order they were created, you could sort them by their task identifiers.

The example above only checks for a 401 response code which is not nearly robust enough, but does demonstrate how one could check for an invalid access token error. In a production application, one would want to check the realm and most likely the www-authenticate header response although it depends on the OAuth2 implementation.
*/






