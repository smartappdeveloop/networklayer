//
//  AlamofirePlay.swift
//  NetworkLayer
//
//  Created by Dmitriy Ignatyev on 12.11.2017.
//  Copyright © 2017 Dmitriy Ignatyev. All rights reserved.
//

import Foundation
import Alamofire



func alamofirePlay() {
  let successResult = Result.success(5)
  let failureResult = Result<Int>.failure(BackendError.network(error: NSError(domain: "", code: 4, userInfo: nil)))
  
}


// For example, here's a simple BackendError enum which will be used in later examples:

typealias NetworkError = NetworkErrorBar<URLConnectionError, BackendError>

enum NetworkErrorBar<C, B>: Error where C: Error, B: Error {
  case connection(C)
  case backend(B)
  
  var error: Error {
    switch self {
    case .connection(let error): return error
    case .backend(let error): return error
    }
  }
  
  func test() {
    error._code
    error._domain
  }
  
}

// TODO: Взять у Гены список ошибок, привести к единому виду.

enum URLConnectionError: Error {
  case connectionLost
  case badConnection
  // ...
}

protocol HumanErrorDescriptionProvidable {
  static func humanDescriptionOf<E>(error: E) -> String where E: Error
}
extension HumanErrorDescriptionProvidable {
  static func humanDescriptionOf<E>(error: E, describer: (E) -> String) -> String where E: Error {
    return describer(error)
  }
}

enum BackendError: Error {
  case network(error: Error) // Capture any underlying Error from the URLSession API
  case dataSerialization(error: Error)
  case jsonSerialization(error: Error)
  case xmlSerialization(error: Error)
  case objectSerialization(reason: String)
  
  struct HumanDescriptionProvider {}
}

extension BackendError.HumanDescriptionProvider {
  static func humanDescriptionOf<E>(error: E, describer: (E) -> String) -> String where E: Error {
    return describer(error)
  }
  
  static func description() {
    let description = humanDescriptionOf(error: URLConnectionError.badConnection) { error -> String in
      switch error {
      case .badConnection: return "Плохое интернет содеинение"
      case .connectionLost: return "Соединение с интернетом потеряно"
      }
    }
    print(description)
  }
  
}







